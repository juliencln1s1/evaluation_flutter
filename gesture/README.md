## Widget exploré : GestureDetector

Le widget GestureDetector est utilisé dans cette application pour détecter les gestes de l'utilisateur, en particulier le tap.

## Guide utilisateur

    Lancez l'application sur votre appareil.
    Vous verrez un conteneur avec le texte "Appuyez ici".
    Appuyez sur le conteneur.
    Un message s'affichera en bas de l'écran indiquant que vous avez tapé sur le conteneur.