class InventoryUtils {
  static void sortByName(Map<String, int> inventory) {
    inventory = Map.fromEntries(
      inventory.entries.toList()
        ..sort((a, b) => a.key.compareTo(b.key)),
    );
  }

  static void sortByQuantity(Map<String, int> inventory) {
    inventory = Map.fromEntries(
      inventory.entries.toList()
        ..sort((a, b) => a.value.compareTo(b.value)),
    );
  }
}