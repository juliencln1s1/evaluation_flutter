import 'package:flutter/material.dart';
import 'home_page.dart';
import 'resource.dart';

class ResourceWidget extends StatefulWidget {
  final Resource resource;
  final Function() onMinePressed;

  ResourceWidget({required this.resource, required this.onMinePressed});

  @override
  _ResourceWidgetState createState() => _ResourceWidgetState();
}

class _ResourceWidgetState extends State<ResourceWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          widget.resource.quantity++;
        });
      },
      child: Container(
        margin: EdgeInsets.all(8),
        padding: EdgeInsets.all(16),
        color: Colors.blue,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              widget.resource.name,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 8),
            Text(
              'Quantité: ${widget.resource.quantity}',
              style: TextStyle(fontSize: 14),
            ),
            SizedBox(height: 8),
            ElevatedButton(
              onPressed: widget.onMinePressed,
              child: Text('Miner'),
            ),
          ],
        ),
      ),
    );
  }
}
