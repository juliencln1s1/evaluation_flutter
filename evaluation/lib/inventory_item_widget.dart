import 'package:flutter/material.dart';

class InventoryItemWidget extends StatelessWidget {
  final String name;
  final int quantity;

  InventoryItemWidget({required this.name, required this.quantity});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(name),
      subtitle: Text('Quantité produite: $quantity'),
    );
  }
}
