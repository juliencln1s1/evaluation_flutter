import 'package:flutter/material.dart';
import 'inventory_item_widget.dart';

class InventoryPage extends StatelessWidget {
  final Map<String, int> inventory;

  InventoryPage({required this.inventory});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inventaire'),
      ),
      body: ListView.builder(
        itemCount: inventory.length,
        itemBuilder: (context, index) {
          String recipeName = inventory.keys.elementAt(index);
          int quantity = inventory[recipeName]!;

          return InventoryItemWidget(name: recipeName, quantity: quantity);
        },
      ),
    );
  }
}
