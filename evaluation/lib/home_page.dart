import 'dart:async';
import 'package:flutter/material.dart';
import 'inventory_page.dart';
import 'recipes_page.dart';
import 'resource.dart';
import 'resource_widget.dart';
import 'recipe.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int totalResourcesMined = 0; // quantité totale de ressource récoltée

  RecipesPage? recipesPage;
  
  // Liste resources
  List<Resource> resources = [
    Resource('Bois', 0),
    Resource('Minerai de fer', 0),
    Resource('Minerai de cuivre', 0),
  ];

  // Liste de recettes
  List<Recipe> recipes = [
    Recipe('Hache', 'Un outil utile', {'Bois': 2, 'Tige en métal': 2}),
    Recipe('Pioche', 'Un outil utile', {'Bois': 2, 'Tige en métal': 3}),
    Recipe('Lingot de fer', 'Un lingot de fer pur', {'Minerai de fer': 1}),
    Recipe('Plaque de fer', 'Une plaque de fer pour la construction', {'Minerai de fer': 2}),
    Recipe('Lingot de cuivre', 'Un lingot de cuivre pur', {'Minerai de cuivre': 1}),
    Recipe('Tige en métal', 'Une tige de métal', {'Lingot de fer': 1}),
    Recipe('Fil électrique', 'Un fil électrique pour fabriquer des composants électroniques', {'Lingot de cuivre': 1}),
    Recipe('Mineur', 'Un bâtiment qui permet d\'automatiser le minage', {'Fil électrique': 5, 'Plaque de fer': 10}),
    Recipe('Fonderie', 'Un bâtiment qui permet d\'automatiser la production.', {'Fil électrique': 5, 'Tige en métal': 8}),
  ];

  Map<String, int> inventory = {};

  late Timer _timer;

  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      mineResourcesAutomatically();
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void mineResourcesAutomatically() {
    // Vérifie si le Mineur est présent dans l'inventaire
    if (inventory.containsKey('Mineur')) {
      // Trouve la ressource 'Minerai de fer' et augmentez la quantité de 1
      for (var res in resources) {
        if (res.name == 'Minerai de fer') {
          res.quantity++;
          setState(() {}); // Met à jour l'interface graphique
          break;
        }
      }
    }

    // Vérifie si la Fonderie est présente dans l'inventaire
    if (inventory.containsKey('Fonderie')) {
      // Convertis les minerai de fer en lingots de fer
      convertResources('Minerai de fer', 'Lingot de fer');
      // Convertis les minerai de cuivre en lingots de cuivre
      convertResources('Minerai de cuivre', 'Lingot de cuivre');
    }
  }

  void convertResources(String sourceResource, String destinationResource) {
    // Trouve la ressource source dans la liste des ressources
    var source = resources.firstWhere((res) => res.name == sourceResource, orElse: () => Resource(sourceResource, 0));
    // Trouve la ressource de destination dans la liste des ressources
    var destination = resources.firstWhere((res) => res.name == destinationResource, orElse: () => Resource(destinationResource, 0));

    // Vérifie si la quantité de la ressource source est supérieure à 0
    if (source.quantity > 0) {
      // Convertis un unité de la ressource source en une unité de la ressource destination
      source.quantity--;
      destination.quantity++;

      // Ajoute la ressource de destination à l'inventaire
      if (inventory.containsKey(destinationResource)) {
        // Si la ressource de destination est déjà dans l'inventaire, met à jour la quantité
        inventory[destinationResource] = inventory[destinationResource]! + 1;
      } else {
        // Sinon, ajoute la ressource de destination à l'inventaire avec une quantité de 1
        inventory[destinationResource] = 1;
      }
      setState(() {}); // Met à jour l'interface graphique
    }
  }

  void produceRecipe(Recipe recipe) {
    for (var resource in recipe.cost.keys) {
      int requiredQuantity = recipe.cost[resource]!;
      // Décrémente la quantité de la ressource dans la liste resources
      for (var res in resources) {
        if (res.name == resource) {
          res.quantity -= requiredQuantity;
          break;
        }
      }
    }
    // Ajoute la recette produite à la liste inventory
    if (inventory.containsKey(recipe.name)) {
      // Si la recette est déjà dans l'inventaire, mets à jour de la quantité
      inventory[recipe.name] = inventory[recipe.name]! + 1;
    } else {
      // Sinon, ajoute la recette à l'inventaire avec une quantité de 1
      inventory[recipe.name] = 1;
    }

    // Mets à jour le nombre total de lingots de fer et de cuivre fabriqués
    int totalIronIngots = inventory['Lingot de fer'] ?? 0;
    int totalCopperIngots = inventory['Lingot de cuivre'] ?? 0;

    // Vérifie les conditions pour débloquer le charbon
    if (totalIronIngots >= 1000 && totalCopperIngots >= 1000 && !resources.any((res) => res.name == 'Charbon')) {
      // Ajoute la nouvelle ressource (charbon) à la liste resources
      resources.add(Resource('Charbon', 0));
    }

    if (recipe.name == 'Mineur') {
      // Initialise la quantité de 'Minerai de fer' pour le nouveau Mineur
      for (var res in resources) {
        if (res.name == 'Minerai de fer') {
          res.quantity = 0;
          break;
        }
      }
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ressources'),
        actions: [
          IconButton(
            icon: Icon(Icons.book),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => RecipesPage(
                    recipes: recipes,
                    resources: resources,
                    produceRecipe: produceRecipe,
                    inventory: inventory,
                  ),
                ),
              );
            },
          ),
          IconButton(
            icon: Icon(Icons.inventory),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => InventoryPage(inventory: inventory)),
              );
            },
          ),
        ],
      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 4,
        ),
        itemBuilder: (context, index) {
          return ResourceWidget(
            resource: resources[index],
            onMinePressed: () {
              setState(() {
                resources[index].quantity++;
                totalResourcesMined++;
              });
            },
          );
        },
        itemCount: resources.length,
      ),
    );
  }
}
