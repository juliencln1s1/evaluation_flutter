class Recipe {
  String name;
  String description;
  Map<String, int> cost;

  Recipe(this.name, this.description, this.cost);

  String costToString() {
    // Convertir le coût en ressources en une chaîne lisible
    List<String> costList = [];
    cost.forEach((resource, quantity) {
      costList.add('$resource: $quantity');
    });
    return costList.join(', ');
  }
}
