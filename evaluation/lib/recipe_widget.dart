import 'package:flutter/material.dart';
import 'recipe.dart';
import 'resource.dart';

class RecipeWidget extends StatelessWidget {
  final Recipe recipe;
  final List<Resource> resources;
  final Function(Recipe) produceRecipe;
  final Map<String, int> inventory;

  RecipeWidget({
    required this.recipe,
    required this.resources,
    required this.produceRecipe,
    required this.inventory,
  });

  @override
  Widget build(BuildContext context) {
    bool canProduce = canProduceRecipe(inventory);

    return Card(
      margin: EdgeInsets.all(8),
      child: ListTile(
        title: Text(recipe.name),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(recipe.description),
            Text('Coût en ressources: ${recipe.costToString()}'),
          ],
        ),
        trailing: ElevatedButton(
          onPressed: canProduce ? () => produceRecipe(recipe) : null, 
          style: ElevatedButton.styleFrom(
            backgroundColor: canProduce ? null : Colors.grey,
          ),
          child: Text('Produire'),
        ),
      ),
    );
  }

  bool canProduceRecipe(Map<String, int> inventory) {
    for (var resource in recipe.cost.keys) {
      int requiredQuantity = recipe.cost[resource]!;

      if (!resourceExists(resource) || getResourceQuantity(resource, inventory) < requiredQuantity) {
        return false;
      }
    }
    return true;
}

  bool resourceExists(String resourceName) {
    return resources.any((res) => res.name == resourceName) || inventory.containsKey(resourceName);
  }

  int getResourceQuantity(String resourceName, Map<String, int> inventory) {
    for (var resource in resources) {
      if (resource.name == resourceName) {
        return resource.quantity;
      }
    }
    return inventory[resourceName] ?? 0;
  }
}
