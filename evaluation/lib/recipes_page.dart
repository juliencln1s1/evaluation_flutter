import 'package:flutter/material.dart';
import 'recipe_widget.dart';
import 'recipe.dart';
import 'resource.dart';

class RecipesPage extends StatelessWidget {
  final List<Recipe> recipes;
  final List<Resource> resources;
  final Function(Recipe) produceRecipe;
  final Map<String, int> inventory;

  RecipesPage({
    required this.recipes,
    required this.resources,
    required this.produceRecipe,
    required this.inventory,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Recettes'),
      ),
      body: ListView.builder(
        itemCount: recipes.length,
        itemBuilder: (context, index) {
          return RecipeWidget(
            recipe: recipes[index],
            resources: resources,
            produceRecipe: produceRecipe,
            inventory: inventory,
          );
        },
      ),
    );
  }
}
