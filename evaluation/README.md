# Installation

Pour lancer le projet :

    - Télécharger le dossier "evaluation"

    - Dans le terminal, se placer dans le repertoire evaluation puis executer la commande "flutter run"

## Les fonctionnalités

Le jeu respectes toutes les demandes :

    - Le joueur peut miner du bois, du fer et du cuivre manuellement tout en voyant évoluer la quantité de ces ressources.

    - Il dispose d'une page d'inventaire où on y trouve le récapitulatif des produits fabriqués.

    - Dans la page des recette il peut y produire n'importe quel element d'inventaire en fonction des ressources minées et de son inventaire.

    - Lorsque 1000 lingots de fer et 1000 lingots de cuivre sont présent dans l'inventaire, le joueur pourra à présent miner du charbon.

    - Lorsque le joueur possède un mineur, l'automatisation du minage du fer se lance, et chaque seconde un minerai de fer est miné.

    - Lorsque le joueur possède une fonderie, l'automatisation des transformations des minerai de fer et de cuivre en lingot de fer et de cuivre se lance, à raison d'un minerai de fer et de cuivre par seconde.

## Difficultés

J'ai rencontré une difficulté à laquelle je n'ai pas eu le temps de faire face, c'est le rechargement de la page Recette. 

Ce qui fait que les boutons "Produire" ne se mettent pas à jour et donc pas en mode "disabled" lorsque les quantités ne sont plus disponible. 

Le joueur est obligé de faire un aller retour manuellement afin de voir où il en est dans ses recettes disponible.